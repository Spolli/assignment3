# Assignment3
Bruno Palazzi 806908

## Istruzioni per l'uso
1)Installare ed eseguire MySQL connettendosi con le proprie credenziali.

2)Eseguire il comando "CREATE DATABASE mydb;"

3)Scaricare la Repository.

4)Su Eclipse EE importare come progetto JPA la cartella "Assignemnt3".

5)Modificare nel file \src\META-INF\persistence.xml USERNAME e PSW con le proprie credenziali della connessione MySQL.

6)Avviare le classi \src\Programma per avviare i test sulle operazioni di creazione, aggiornamento e rimozione relative ad ogni entità.

7)Verificare che tutti i test abbiano dato esito positivo; in caso contrario potrebbero esserci stati problemi interni alla gestione delle variabili dell'IDE Eclipse, rieseguire quindi i test.

8)Aprire la propria connesisone MySql, eseguire i comando "USE mydb;" per aprire il db appena creato popolato e testato.

9)Verificare che i dati delle Entità create sono stati inseriti correttamente nel db effettuando delle semplici query sulle tabelle dello schema.
