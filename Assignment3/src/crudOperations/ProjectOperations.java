package crudOperations;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;
import Classes.Project;
import Classes.Department;
import Classes.Employee;


public class ProjectOperations {
public static Project getProject(String ProjectId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Project p = new Project();
		em.getTransaction().begin();
		p = em.find(Project.class, ProjectId);
		if (p == null)
			return p;
		em.merge(p);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return p;
	}

	public static List <Project> getProjectsByLocation(String Location) {
		
		List <Project> l = DbOperations.selectAllProjects();
		List <Project> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getLocation().equals(Location))
				l1.add(l.get(i));
		}
		return l1;
	}
	
	public static List <Project> getProjectsByControllingDepartment(Department controllingDepartment) {
			
		List <Project> l = DbOperations.selectAllProjects();
		List <Project> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getControllingDepartment().getCode().equals(controllingDepartment.getCode()))
				l1.add(l.get(i));
		}
		return l1;
		}

	public static int createProject(Project project) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		em.merge(project);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
	}


	public static int updateProjectName(String ProjectId, String newName) {
		if (getProject(ProjectId) == null || newName.length()>20 || newName.length()==0 || newName == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			
			Project p = new Project();
			em.getTransaction().begin();
			p = em.find(Project.class, ProjectId);
			p.setName(newName);
			em.merge(p);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}
	}

	public static int updateProjectLocation(String ProjectId, String newLocation) {
		if (getProject(ProjectId) == null || newLocation.length()>20 || newLocation.length()==0 || newLocation == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			
			Project p = new Project();
			em.getTransaction().begin();
			p = em.find(Project.class, ProjectId);
			p.setLocation(newLocation);
			em.merge(p);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
	
	public static int updateProjectDepartment(String ProjectId, Department d) {
		if (getProject(ProjectId) == null || d == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			
			Project p = new Project();
			try {		
				em.getTransaction().begin();
				p = em.find(Project.class, ProjectId);
				p.setControllingDepartment(d);
				em.merge(p);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception e) {
				return -1;
			}
		}		
	}
	public static int updateProjectEmployees(String ProjectId, List<Employee> employees) {
		if (getProject(ProjectId) == null || employees == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			
			Project p = new Project();
			try {		
				em.getTransaction().begin();
				p = em.find(Project.class, ProjectId);				
				p.setEmployees(employees);
				em.merge(p);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception e) {
				return -1;
			}
		}		
	}
	
	public static int removeProject(String ProjectId) {
		if (getProject(ProjectId) == null) {
			return -1;
		}
		else {			
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			try {
				em.getTransaction().begin();
				Project p = em.find(Project.class, ProjectId);
				if (!em.contains(p)) {
				    p = em.merge(p);
				}
				p.setControllingDepartment(null);
				p.setEmployees(null);
				em.remove(p);
				em.getTransaction().commit();
				em.close();
				factory.close();			
				return 1;
			}catch(Exception e) {
				return -1;
			}
		}
	}

}
