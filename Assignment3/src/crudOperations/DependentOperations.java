package crudOperations;

import javax.persistence.*;

import Classes.Department;
import Classes.Dependent;
import Classes.Employee;
import Classes.Project;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;


public class DependentOperations {
	
	public static Dependent getDependent(String DependentId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Dependent w = new Dependent();
		em.getTransaction().begin();
		w = em.find(Dependent.class, DependentId);
		if (w == null)
			return w;
		em.merge(w);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return w;
	}

	public static int createDependent(Dependent dependent) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.merge(dependent);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return 1;
	}
	
	
	public static List <Dependent> getDependentsByFirstName(String name) {
		
		List <Dependent> l = DbOperations.selectAllDependents();
		List <Dependent> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getFirstName().equals(name))
				l1.add(l.get(i));
		}
		return l1;
	}
	
	public static List <Dependent> getDependentsByEmployee(Employee employee) {
			
		List <Dependent> l = DbOperations.selectAllDependents();
		List <Dependent> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getEmployee().getSSN().equals(employee.getSSN()))
				l1.add(l.get(i));
		}
		return l1;
	}

	public static int updateDependentFirstName(String DependentId, String newFirstName) {
		if (getDependent(DependentId) == null || newFirstName.length()>20 || newFirstName.length()==0 || newFirstName== null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			
			Dependent w = new Dependent();
			w = em.find(Dependent.class, DependentId);
			
			em.getTransaction().begin();
			w.setFirstName(newFirstName);
			em.merge(w);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}
	}
	
	public static int updateDependentLastName(String DependentId, String newLastName) {
		if (getDependent(DependentId) == null || newLastName.length()>20 || newLastName.length()==0 || newLastName == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Dependent w = new Dependent();
			w = em.find(Dependent.class, DependentId);
			em.getTransaction().begin();
			w.setLastName(newLastName);
			em.merge(w);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}
	}

	public static int updateDependentDOB(String DependentId, GregorianCalendar DOB) {
		if (getDependent(DependentId) == null || DOB == null)
			return -1;
		else {
			
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Dependent w = new Dependent();
			w = em.find(Dependent.class, DependentId);
			em.getTransaction().begin();
			w.setDateOfBirth(DOB);
			em.merge(w);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
	
	public static int updateDependentEmployee(String DependentId, Employee newEmployee) {
		if (getDependent(DependentId) == null || newEmployee == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Dependent w = new Dependent();
			try {		
				em.getTransaction().begin();
				w = em.find(Dependent.class, DependentId);
				w.setEmployee(newEmployee);
				em.merge(w);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception e) {
				return -1;
			}
		}		
	}
	
	public static int removeDependent(String DependentId) {
		if (getDependent(DependentId) == null) {
			return -1;
		}
		else {			
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			try {
				em.getTransaction().begin();
				Dependent w = em.find(Dependent.class, DependentId);
				if (!em.contains(w)) {
				    w = em.merge(w);
				}
				w.setEmployee(null);
				em.remove(w);
				em.getTransaction().commit();
				em.close();
				factory.close();			
				return 1;
			}catch(Exception e) {
				return -1;
			}
		}
	}

}
