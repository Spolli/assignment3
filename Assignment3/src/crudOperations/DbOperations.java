package crudOperations;
import Classes.*;

import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.*;

public class DbOperations {
	
	public static int deleteAllData() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		try {
		em.getTransaction().begin();
		
		query = em.createQuery("ALTER TABLE Classes.Department d\n" + 
				"DROP FOREIGN KEY manager;" );
		query.executeUpdate();
		query = em.createQuery("DELETE * FROM Classes.Department d;");
		query.executeUpdate();
		query = em.createQuery("ALTER TABLE Classes.Employee e\n" + 
				"DROP FOREIGN KEY department;" +
				"DROP FOREIGN KEY supervisor;" +
				"DROP FOREIGN KEY projects;");
		query.executeUpdate();
		query = em.createQuery("DELETE * FROM Classes.Employee e;");
		query.executeUpdate();
		query = em.createQuery("ALTER TABLE Classes.Project p\n" + 
				"DROP FOREIGN KEY controllingDepartment;" +
				"DROP FOREIGN KEY employees;");
		query.executeUpdate();
		query = em.createQuery("DELETE * FROM Classes.Project p;");
		query.executeUpdate();
		query = em.createQuery("ALTER TABLE Classes.Dependent d\n" + 
				"DROP FOREIGN KEY employee;" );
		query.executeUpdate();
		query = em.createQuery("DELETE * FROM Classes.Dependent d;");
		query.executeUpdate();
		query = em.createQuery("DROP DATABASE mydb;");
		query.executeUpdate();
		query = em.createQuery("CREATE DATABASE mydb;");
		query.executeUpdate();
		query = em.createQuery("USE mydb;");
		query.executeUpdate();
		em.getTransaction().commit();
		em.close();
		factory.close();
		return 1;
		}
		catch (Exception e) {
			return -1;
		}

	}
	
	public static int[] insertAllData() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		try {
			Dependent W1 = new Dependent("GLLMRC07E65F456V", "Marco", "Galli", new GregorianCalendar(1965, 05, 07), null);
			Dependent W2 = new Dependent("DRGFRC12E85A456V", "Franco", "Dorga", new GregorianCalendar(1985, 25, 12), null);
			Dependent W3 = new Dependent("PNGMRO45E54B264A", "Mario", "Panga", new GregorianCalendar(1976, 31, 07), null);
			Dependent W4 = new Dependent("DNZMRL42T45T243B", "Marcello", "Danza", new GregorianCalendar(1958, 15, 02), null);
			Dependent W5 = new Dependent("VLLFB012R43B556J", "Fabio", "Villa", new GregorianCalendar(1966, 15, 04), null);
			Dependent W6 = new Dependent("SCCSTF23G44N423H", "Stefano", "Sacco", new GregorianCalendar(1978, 12, 12), null);
			Dependent W7 = new Dependent("RILMNO34R54N242K", "Mario", "Raiola", new GregorianCalendar(1986, 15, 11), null);
			Department D1 = new Department("MB", "Mondial Bank", "New York", 2345, null);
			Department D2 = new Department("ER", "Entity-Relationship", "London", 420, null);
			Department D3 = new Department("SR", "Scientific Research", "Paris", 530, null);
			Department D4 = new Department("RT", "Radio Technology", "Jakarta", 1200, null);
			Department D5 = new Department("AF", "Australian Fields", "Auckland", 400, null);
			Project P1 = new Project("MB01", "Transaction system", "New York", D1, null);
			Project P2 = new Project("MB02", "User Interface", "New York", D1, null);
			Project P3 = new Project("MB03", "Security Task", "Boston", D1, null);
			Project P4 = new Project("ER01", "Modelling system", "London", D2, null);
			Project P5 = new Project("SR01", "Cancer Prevention", "Paris", D3, null);
			Project P6 = new Project("SR02", "DNA coding", "Paris", D3, null);
			Project P7 = new Project("RT01", "New wave system", "Jakarta", D4, null);
			Project P8 = new Project("RT02", "Radio Waves", "Bali", D4, null);
			LinkedList<Project> L1 = new LinkedList<Project>();
			LinkedList<Project> L2 = new LinkedList<Project>();
			LinkedList<Project> L3 = new LinkedList<Project>();
			LinkedList<Project> L4 = new LinkedList<Project>();
			LinkedList<Project> L5 = new LinkedList<Project>();
			LinkedList<Project> L6 = new LinkedList<Project>();
			L1.add(P1);
			L1.add(P2);
			L1.add(P3);
			L2.add(P2);
			L2.add(P3);
			L2.add(P4);
			L3.add(P4);
			L3.add(P5);
			L4.add(P5);
			L4.add(P6);
			L5.add(P6);
			L5.add(P7);
			L6.add(P6);
			L6.add(P7);
			Employee E1 = new Employee("DNGMRC05F77E796P", "Marco", "Dunga", new GregorianCalendar(1977, 04, 01), "Male", "23rd Garibaldi Avenue", 52352, D1, null, L1 );
			Employee E2 = new Employee("CRSSMN02E87R294O", "Simone", "Corso", new GregorianCalendar(1987, 02, 05), "Male", "42nd Marco Polo Street", 42000, D1, E1, L2);
			Employee E3 = new Employee("SNLSRN05B66E856P", "Serena", "Sangalli", new GregorianCalendar(1966, 05, 02), "Female", "3rd Breda Avenue", 65768, D1, null, L3);
			Employee E4 = new Employee("DRGFRC12I88E365L", "Francesca", "Dorigato", new GregorianCalendar(1988, 12, 12), "Female", "2nd Garibaldi Avenue", 45245, D2, E3, L4);
			Employee E5 = new Employee("MSTSLV27I59E745T", "Silvia", "Mistri", new GregorianCalendar(1959, 03, 27), "Female", "21st D. Alighieri Avenue", 52865, D3, null, L5);
			Employee E6 = new Employee("GNDLCU14M65E867N", "Luca", "Gandolfi", new GregorianCalendar(1965, 11, 14), "Male", "31st Zara Avenue", 78459, D4, null, L6);
			Employee E7 = new Employee("GLLETR13C94W453G", "Ettore", "Galli", new GregorianCalendar(1965, 10, 15), "Male", "31st Loreto Square", 84558, D4, E1, L6);
			E1.setSupervisor(E1);
			E3.setSupervisor(E3);
			E5.setSupervisor(E5);
			E6.setSupervisor(E6);
			D1.setManager(E1);
			D2.setManager(E3);
			D3.setManager(E5);
			D4.setManager(E6);
			W1.setEmployee(E1);
			W2.setEmployee(E1);
			W3.setEmployee(E2);
			W4.setEmployee(E3);
			W5.setEmployee(E4);
			W6.setEmployee(E5);
			W7.setEmployee(E6);
			List<Employee> GP1 = new LinkedList<Employee>();
			List<Employee> GP2 = new LinkedList<Employee>();
			List<Employee> GP3 = new LinkedList<Employee>();
			List<Employee> GP4 = new LinkedList<Employee>();
			List<Employee> GP5 = new LinkedList<Employee>();
			List<Employee> GP6 = new LinkedList<Employee>();
			List<Employee> GP7 = new LinkedList<Employee>();
			GP1.add(E1);
			GP2.add(E1);
			GP2.add(E2);
			GP3.add(E1);
			GP3.add(E2);
			GP4.add(E2);
			GP4.add(E3);
			GP5.add(E3);
			GP5.add(E4);
			GP6.add(E4);
			GP6.add(E5);
			GP6.add(E6);
			GP7.add(E5);
			GP7.add(E6);
			P1.setEmployees(GP1);
			P2.setEmployees(GP2);
			P3.setEmployees(GP3);
			P4.setEmployees(GP4);
			P5.setEmployees(GP5);
			P6.setEmployees(GP6);
			P7.setEmployees(GP7);
			int projects = 0;
			int employees = 0;
			int departments = 0;
			int dependents = 0;
			projects += ProjectOperations.createProject(P1);
			projects += ProjectOperations.createProject(P2);
			projects += ProjectOperations.createProject(P3);
			projects += ProjectOperations.createProject(P4);
			projects += ProjectOperations.createProject(P5);
			projects += ProjectOperations.createProject(P6);
			projects += ProjectOperations.createProject(P7);
			projects += ProjectOperations.createProject(P8);
			employees += EmployeeOperations.createEmployee(E1);
			employees += EmployeeOperations.createEmployee(E2);
			employees += EmployeeOperations.createEmployee(E3);
			employees += EmployeeOperations.createEmployee(E4);
			employees += EmployeeOperations.createEmployee(E5);
			employees += EmployeeOperations.createEmployee(E6);
			employees += EmployeeOperations.createEmployee(E7);
			departments += DepartmentOperations.createDepartment(D1);
			departments += DepartmentOperations.createDepartment(D2);
			departments += DepartmentOperations.createDepartment(D3);
			departments += DepartmentOperations.createDepartment(D4);
			departments += DepartmentOperations.createDepartment(D5);
			dependents += DependentOperations.createDependent(W1);
			dependents += DependentOperations.createDependent(W2);
			dependents += DependentOperations.createDependent(W3);
			dependents += DependentOperations.createDependent(W4);
			dependents += DependentOperations.createDependent(W5);
			dependents += DependentOperations.createDependent(W6);
			dependents += DependentOperations.createDependent(W7);
			return new int[] {projects, employees, departments, dependents};
		}
		catch (Exception e) {
			return null;
		}

	}
	
	public static List<Department> selectAllDepartments() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		
		List<Department> listOfDepartments = (List<Department>) em.createQuery("SELECT d FROM Classes.Department d", Department.class).getResultList();
		
		return listOfDepartments;
	}
	
	public static List<Dependent> selectAllDependents() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		
		List<Dependent> listOfDependents = (List<Dependent>) em.createQuery("SELECT d FROM Classes.Dependent d", Dependent.class).getResultList();
		
		return listOfDependents;
	}

	public static List<Employee> selectAllEmployees() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		
		List<Employee> listOfEmployees = (List<Employee>) em.createQuery("SELECT e FROM Classes.Employee e", Employee.class).getResultList();
		
		return listOfEmployees;
	}
	
	public static List<Project> selectAllProjects() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		
		List<Project> listOfProjects = (List<Project>) em.createQuery("SELECT p FROM Classes.Project p", Project.class).getResultList();
		
		return listOfProjects;
	}
	
	public static List<Dependent> selectAllEmployeesDependents(String SSN) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		query = em.createQuery("SELECT d FROM Classes.Dependent d WHERE d.Employee = :SSN", Dependent.class);
		query.setParameter("SSN", EmployeeOperations.getEmployee(SSN));
		List<Dependent> dependents = (List<Dependent>) query.getResultList();
		return dependents;
	}
	
	public static List<Project> selectAllEmployeesProjects(String SSN) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		query = em.createQuery("SELECT p FROM Classes.Project p WHERE p.Employee = :SSN", Project.class);
		query.setParameter("SSN", EmployeeOperations.getEmployee(SSN));
		List<Project> projects = (List<Project>) query.getResultList();
		return projects;
	}
	
	public static List<Employee> selectAllDepartmentEmployees(String Code) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		query = em.createQuery("SELECT e FROM Classes.Employee e WHERE e.Department = :Code", Employee.class);
		query.setParameter("SSN", DepartmentOperations.getDepartment(Code));
		List<Employee> employees = (List<Employee>) query.getResultList();
		return employees;
	}
	
	public static List<Project> selectAllDepartmentProjects(String Code) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		query = em.createQuery("SELECT p FROM Classes.Project p WHERE p.controllingDepartment = :Code", Project.class);
		query.setParameter("SSN", DepartmentOperations.getDepartment(Code));
		List<Project> projects = (List<Project>) query.getResultList();
		return projects;
	}
	
	public static int deleteAllNullEmployeesDependent(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		try {
			em.getTransaction().begin();
			query = em.createQuery("DELETE FROM Class.Dependent d WHERE d.Employee IS NULL");
			query.executeUpdate();
			em.getTransaction().commit();
			
			return 1;
		}catch (Exception e) {
			return -1;
		}	

	}
	
	public static int deleteAllNullSupervisorEmployees(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		try {
			em.getTransaction().begin();
			query = em.createQuery("DELETE FROM Class.Employee e WHERE e.supervisor IS NULL");
			query.executeUpdate();
			em.getTransaction().commit();
			
			return 1;
		}catch (Exception e) {
			return -1;
		}	

	}
	
	public static int deleteAllNullManagerDepartment(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		try {
			em.getTransaction().begin();
			query = em.createQuery("DELETE FROM Class.Department d WHERE d.manager IS NULL");
			query.executeUpdate();
			em.getTransaction().commit();
			
			return 1;
		}catch (Exception e) {
			return -1;
		}	

	}
	
	public static int deleteAllNullDepartmentProject(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		try {
			em.getTransaction().begin();
			query = em.createQuery("DELETE FROM Class.Project p WHERE p.controllingDepartment IS NULL");
			query.executeUpdate();
			em.getTransaction().commit();
			
			return 1;
		}catch (Exception e) {
			return -1;
		}	

	}
	
	public static int deleteAllNullDepartmentEmployee(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		try {
			em.getTransaction().begin();
			query = em.createQuery("DELETE FROM Class.Employee e WHERE e.department IS NULL");
			query.executeUpdate();
			em.getTransaction().commit();
			
			return 1;
		}catch (Exception e) {
			return -1;
		}	

	}
		
}
