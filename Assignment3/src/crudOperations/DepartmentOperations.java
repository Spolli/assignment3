package crudOperations;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;
import Classes.*;
import crudOperations.DbOperations;

public class DepartmentOperations {
	
	public static Department getDepartment(String departmentId) {
			
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Department d = new Department();
			em.getTransaction().begin();
			d = em.find(Department.class, departmentId);
			if (d == null)
				return d;
			em.merge(d);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return d;
		}

	public static List <Department> getDepartmentsByLocation(String Location) {
		
		List <Department> l = DbOperations.selectAllDepartments();
		List <Department> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getLocation().equals(Location))
				l1.add(l.get(i));
		}
		return l1;
	}
	
	public static List <Department> getDepartmentsWithAtLeastNEmployee(long number) {
			
		List <Department> l = DbOperations.selectAllDepartments();
		List <Department> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getEmployeeNumber() >= number)
				l1.add(l.get(i));
		}
		return l1;
		}
	
	public static int createDepartment(Department department) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		
			em.getTransaction().begin();
			em.merge(department);
			em.getTransaction().commit();
			em.close();
			factory.close();
			
			return 1;
	}
	
	public static int updateDepartmentName(String departmentId, String newName) {
		if (getDepartment(departmentId) == null || newName.length()>20 || newName.length()==0 || newName == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Department d = new Department();
			d = em.find(Department.class, departmentId);
			em.getTransaction().begin();
			d.setName(newName);
			em.merge(d);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}
	}

	public static int updateDepartmentLocation(String departmentId, String newLocation) {
		if (getDepartment(departmentId) == null || newLocation.length()>20 || newLocation.length()==0 || newLocation == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Department d = new Department();
			d = em.find(Department.class, departmentId);
			em.getTransaction().begin();
			d.setLocation(newLocation);
			em.merge(d);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
	
	public static int updateDepartmentEmployeeNumber(String departmentId, long newNumber) {
		if (getDepartment(departmentId) == null || newNumber < 0)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Department d = new Department();
			d = em.find(Department.class, departmentId);
			em.getTransaction().begin();
			d.setEmployeeNumber(newNumber);
			em.merge(d);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
		
	
	public static int updateDepartmentManager(String departmentId, Employee manager) {
		if (getDepartment(departmentId) == null || manager == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Department d = new Department();
			try {		
				d = em.find(Department.class, departmentId);
				em.getTransaction().begin();
				d.setManager(manager);
				em.merge(d);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception e) {
				return -1;
			}
		}		
	}
	
	public static int removeDepartment(String DepartmentId) {
		if (getDepartment(DepartmentId) == null) {
			return -1;
		}
		else {			
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			try {
				em.getTransaction().begin();
				Department d = em.find(Department.class, DepartmentId);
				if (!em.contains(d)) {
				    d = em.merge(d);
				}
				d.setManager(null);
				em.remove(d);			
				em.getTransaction().commit();
				em.close();
				factory.close();			
				return 1;
			}catch(Exception e) {
				return -1;
			}
		}
	}

}
