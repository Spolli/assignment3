package crudOperations;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;

import Classes.Department;
import Classes.Employee;
import Classes.Project;
import crudOperations.DbOperations;
public class EmployeeOperations {
public static Employee getEmployee(String employeeId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		Employee e = new Employee();
		em.getTransaction().begin();
		e = em.find(Employee.class, employeeId);
		if (e == null)
			return e;
		em.merge(e);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return e;

	}

	public static int createEmployee(Employee employee) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
		EntityManager em = factory.createEntityManager();
		
			em.getTransaction().begin();
			em.merge(employee);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
	}

	public static List <Employee> getEmployeesByGreaterSalary(long salary) {
		List <Employee> l = DbOperations.selectAllEmployees();
		List <Employee> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getSalary() > salary)
				l1.add(l.get(i));
		}
		return l1;
	}
	
	public static List <Employee> getEmployeesByDepartment(Department department) {
		List <Employee> l = DbOperations.selectAllEmployees();
		List <Employee> l1 = new LinkedList();
		for (int i = 0; i< l.size(); i++) {
			if (l.get(i).getDepartment().getCode().equals(department.getCode()))
				l1.add(l.get(i));
		}
		return l1;
	}

	
	public static int updateEmployeeFirstName(String employeeId, String newFirstName) {
		if (getEmployee(employeeId) == null || newFirstName.length()>20 || newFirstName.length()==0 || newFirstName == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			em.getTransaction().begin();
			e = em.find(Employee.class, employeeId);
			e.setFirstName(newFirstName);
			em.merge(e);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}
	}
	
	public static int updateEmployeeLastName(String employeeId, String newLastName) {
		if (getEmployee(employeeId) == null || newLastName.length()>20 || newLastName.length()==0 || newLastName == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			em.getTransaction().begin();
			e = em.find(Employee.class, employeeId);
			e.setLastName(newLastName);
			em.merge(e);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}
	}

	public static int updateEmployeeDOB(String employeeId, GregorianCalendar DOB) {
		if (getEmployee(employeeId) == null || DOB == null || DOB.get(Calendar.MONTH)>12 || DOB.get(Calendar.DAY_OF_MONTH)>31 )
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			em.getTransaction().begin();
			e = em.find(Employee.class, employeeId);
			e.setDateOfBirth(DOB);
			em.merge(e);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
	public static int updateEmployeeSex(String employeeId, String sex) {
		if (getEmployee(employeeId) == null || (sex.toLowerCase() != "male" && sex.toLowerCase() != "female") )
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();		
			em.getTransaction().begin();
			e = em.find(Employee.class, employeeId);
			e.setSex(sex);
			em.merge(e);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
	
	public static int updateEmployeeAddress(String employeeId, String newAddress) {
		if (getEmployee(employeeId) == null|| newAddress.length()>20 || newAddress.length()==0 || newAddress == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			em.getTransaction().begin();
			e = em.find(Employee.class, employeeId);
			e.setAddress(newAddress);
			em.merge(e);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
	
	public static int updateEmployeeSalary(String employeeId, double newSalary) {
		if (getEmployee(employeeId) == null || newSalary < 0)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			em.getTransaction().begin();
			e = em.find(Employee.class, employeeId);
			e.setSalary(newSalary);
			em.merge(e);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}		
	}
	
	public static int updateEmployeeSupervisor(String employeeId, Employee supervisor) {
		if (getEmployee(employeeId) == null || supervisor == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			try {		
				em.getTransaction().begin();
				e = em.find(Employee.class, employeeId);
				e.setSupervisor(supervisor);
				em.merge(e);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception exc) {
				return -1;
			}

			
		}		
	}
	
	public static int updateEmployeeDepartment(String employeeId, Department department) {
		if (getEmployee(employeeId) == null || department == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			try {		
				em.getTransaction().begin();
				e = em.find(Employee.class, employeeId);
				e.setDepartment(department);
				em.merge(e);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception exc) {
				return -1;
			}
		}		
	}
	
	public static int updateEmployeeProjects(String employeeId, List<Project> projects) {
		if (getEmployee(employeeId) == null || projects == null)
			return -1;
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			Employee e = new Employee();
			try {		
				em.getTransaction().begin();
				e = em.find(Employee.class, employeeId);
				e.setProjects(projects);
				em.merge(e);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception exc) {
				return -1;
			}
		}		
	}
	
	public static int removeEmployee(String EmployeeId) {
		if (getEmployee(EmployeeId) == null) {
			return -1;
		}
		else {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assignment3");
			EntityManager em = factory.createEntityManager();
			try {
				em.getTransaction().begin(); 
				Employee e = em.find(Employee.class, EmployeeId);
				if (!em.contains(e)) {
				    e = em.merge(e);
				}
				e.setDepartment(null);
				e.setProjects(null);
				e.setSupervisor(null);
				em.remove(e);
				em.getTransaction().commit();
				em.close();
				factory.close();
				return 1;
			}catch(Exception exc) {
				return -1;
				}
		}
	}

}




