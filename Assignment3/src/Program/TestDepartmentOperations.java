package Program;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import Classes.Department;
import crudOperations.DbOperations;
import crudOperations.DepartmentOperations;
import crudOperations.EmployeeOperations;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TestDepartmentOperations {
	@org.junit.jupiter.api.Test
	public void testInsertDepartment() {
		int [] data = DbOperations.insertAllData();

		assert(data[2] == 5):
			"There was a error during createEmployee operation";
		Department D1 = DepartmentOperations.getDepartment("MB");
		Department D2 = DepartmentOperations.getDepartment("ER");
		Department D3 = DepartmentOperations.getDepartment("SR");
		Department D4 = DepartmentOperations.getDepartment("RT");
		Department D5 = DepartmentOperations.getDepartment("AF");
		
		assert(DepartmentOperations.getDepartment(D1.getCode()) != null):
			D1.getCode() + " department should be in the database!";
		
		assert(DepartmentOperations.getDepartment(D2.getCode()) != null):
			D2.getCode() + " department should be in the database!";
		
		assert(DepartmentOperations.getDepartment(D3.getCode()) != null):
			D3.getCode() + " department should be in the database!";
		
		assert(DepartmentOperations.getDepartment(D4.getCode()) != null):
			D4.getCode() + " department should be in the database!";
		
		assert(DepartmentOperations.getDepartment(D5.getCode()) != null):
			D5.getCode() + " department should be in the database!";
		
		assert(DepartmentOperations.getDepartment("QW") == null):
			"QW	 department doesn't exist!";
		assert(DepartmentOperations.getDepartmentsByLocation("Milan").size() == 0);
		
		assert(DepartmentOperations.getDepartmentsWithAtLeastNEmployee(0).size() == 5);
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testInsertDepartment() PASSED");
	}
	
	@org.junit.jupiter.api.Test
	public void testUpdateDepartment() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[2] == 5):
			"There was a error during createEmployee operation";
		Department D1 = DepartmentOperations.getDepartment("MB");
		
		assert (D1 != null): "A dependent that should be in the db isn't inside!";
		
		assert(DepartmentOperations.updateDepartmentName(D1.getCode(), "MondialBank Research") == 1):
			"Error in updateDepartmentName, output is "+DepartmentOperations.updateDepartmentName(D1.getCode(), "MondialBank Research");

		assert(DepartmentOperations.updateDepartmentName(D1.getCode(), "Mondial Bank Research") == -1):
			"Error in updateDepartmentName, output is "+DepartmentOperations.updateDepartmentName(D1.getCode(), "Mondial Bank Research");

		assert (DepartmentOperations.updateDepartmentLocation(D1.getCode(), "Zurich") == 1):
			"Error in updateDepartmentLocation, output is "+DepartmentOperations.updateDepartmentLocation(D1.getCode(), "Zurich");

		assert (DepartmentOperations.updateDepartmentLocation(D1.getCode(), "Welcome to Burkina Faso country") == -1):
			"Error in updateDepartmentLocation, output is "+DepartmentOperations.updateDepartmentLocation(D1.getCode(), "Welcome to Burkina Faso country");

		assert (DepartmentOperations.updateDepartmentEmployeeNumber(D1.getCode(), 560) == 1):
			"Error in updateDepartmentEmployeeNumber, output is "+DepartmentOperations.updateDepartmentEmployeeNumber(D1.getCode(), 560);

		assert (DepartmentOperations.updateDepartmentEmployeeNumber(D1.getCode(), -55) == -1):
			"Error in updateDepartmentEmployeeNumber, output is "+DepartmentOperations.updateDepartmentEmployeeNumber(D1.getCode(), -55);

		assert (DepartmentOperations.updateDepartmentManager(D1.getCode(), EmployeeOperations.getEmployee("DRGFRC12I88E365L")) == 1):
			"Error in updateDepartmentManager, output is "+DepartmentOperations.updateDepartmentManager(D1.getCode(), EmployeeOperations.getEmployee("DRGFRC12I88E365L"));

		assert (DepartmentOperations.updateDepartmentManager(D1.getCode(), EmployeeOperations.getEmployee("PLZBRN96A28A234D")) == -1):
			"Error in updateDepartmentManager, the manager doesn't exist";

		assert (DepartmentOperations.getDepartment(D1.getCode()).getName().equals("MondialBank Research")):
			"Error: the Department name were not update";

		assert (DepartmentOperations.getDepartment(D1.getCode()).getLocation().equals("Zurich")):
			"Error: the Department location were not update";
		
		assert (DepartmentOperations.getDepartment(D1.getCode()).getEmployeeNumber() == 560):
			"Error: the Department name were not update";

		assert (DepartmentOperations.getDepartment(D1.getCode()).getManager().getSSN().equals("DRGFRC12I88E365L")):
			"Error: the Department manager were not update";

		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testUpdateDepartment() PASSED");	
	}
	
	@org.junit.jupiter.api.Test
	public void testRemoveDepartment() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[2] == 5):
			"There was a error during createEmployee operation";
		Department D3 = DepartmentOperations.getDepartment("SR");
		Department D5 = DepartmentOperations.getDepartment("AF");
		
		
		assert (D5 != null): "A Department that should be in the db isn't inside!";
		
		assert (DepartmentOperations.removeDepartment(D5.getCode()) == 1):
			"A error occured during remove department operation";
		
		assert (DepartmentOperations.removeDepartment(D5.getCode()) == -1):
			"This Department has already been removed";
		
		assert (DepartmentOperations.removeDepartment(D3.getCode()) == -1):
			"This Department can't be deleted because it'related to other entities";
		
		assert (DepartmentOperations.removeDepartment("FK") == -1):
			"FK Department doesn't exist";
		
		assert (DepartmentOperations.getDepartment(D5.getCode()) == null):
			D5.getCode() + " " + D5.getName() + ": this Department should not be in the db";
		
		System.out.println("testemoveDepartment() PASSED");	
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
	}
}
