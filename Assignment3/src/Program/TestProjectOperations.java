package Program;

import java.util.LinkedList;
import java.util.List;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import Classes.Employee;
import Classes.Project;
import crudOperations.*;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TestProjectOperations {
	@org.junit.jupiter.api.Test
	public void testInsertProject() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[0] == 8):
			"There was a error during createEmployee operation";
		Project P1 =  ProjectOperations.getProject("MB01");
		Project P2 =  ProjectOperations.getProject("MB02");
		Project P3 =  ProjectOperations.getProject("MB03");
		Project P4 =  ProjectOperations.getProject("ER01");
		Project P5 =  ProjectOperations.getProject("SR01");
		Project P6 =  ProjectOperations.getProject("SR02");
		Project P7 =  ProjectOperations.getProject("RT01");
		Project P8 =  ProjectOperations.getProject("RT02");
		
		assert(ProjectOperations.getProject(P1.getCode()) != null):
			P1.getCode() + " " +P1.getName() + " " +P1.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject(P2.getCode()) != null):
			P2.getCode() + " " +P2.getName() + " " +P2.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject(P3.getCode()) != null):
			P3.getCode() + " " +P3.getName() + " " +P3.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject(P4.getCode()) != null):
			P4.getCode() + " " +P4.getName() + " " +P4.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject(P5.getCode()) != null):
			P5.getCode() + " " +P5.getName() + " " +P5.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject(P6.getCode()) != null):
			P6.getCode() + " " +P6.getName() + " " +P6.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject(P7.getCode()) != null):
			P7.getCode() + " " +P7.getName() + " " +P7.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject(P8.getCode()) != null):
			P8.getCode() + " " +P8.getName() + " " +P8.getLocation() + " project should be in the database!";
		
		assert(ProjectOperations.getProject("TR324") == null):
			"TR324 project shouldn't be in the database!";
		
		assert(ProjectOperations.getProjectsByLocation("New York").size() == 2);
		
		assert(ProjectOperations.getProjectsByControllingDepartment(DepartmentOperations.getDepartment("MB")).size() == 3 );
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testInsertProject() PASSED");
	}

	@org.junit.jupiter.api.Test
	public void testUpdateProject() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[0] == 8):
			"There was a error during createEmployee operation";
		Project P1 =  ProjectOperations.getProject("MB01");
		Project P2 =  ProjectOperations.getProject("MB02");

		assert (P2 != null):
			"A Project that should be in the db isn't inside!";

		assert(ProjectOperations.updateProjectName(P2.getCode(), "MB42") == 1):
			"Error in updateProjectFirstName, output is "+ProjectOperations.updateProjectName(P1.getCode(), "MB42");

		//Name too long
		assert(ProjectOperations.updateProjectName(P2.getCode(), "MBERTSRUT4EIRTHYROROITURT42") == -1):
			"Error in updateProjectFirstName, newName too long";

		assert (ProjectOperations.updateProjectLocation(P2.getCode(), "Zurich") == 1):
			"Error in updateEmployeeLocation, output is "+ProjectOperations.updateProjectLocation(P1.getCode(), "Zurich");

		assert (ProjectOperations.updateProjectLocation(P2.getCode(), "Welcome to Burkina Faso country") == -1):
			"Error in updateEmployeeLocation, newLocation is too long";

		assert (ProjectOperations.updateProjectDepartment(P2.getCode(), DepartmentOperations.getDepartment("ER")) == 1):
			"Error in updateEmployeeDepartment, output is "+ProjectOperations.updateProjectDepartment(P1.getCode(), DepartmentOperations.getDepartment("ER"));

		assert (ProjectOperations.updateProjectDepartment(P1.getCode(), DepartmentOperations.getDepartment("QW")) == -1):
			"Error in updateEmployeeDepartment, the department doesn't exist";

		List <Employee> employees = new LinkedList();
		employees.add(EmployeeOperations.getEmployee("SNLSRN05B66E856P"));
		employees.add(EmployeeOperations.getEmployee("GNDLCU14M65E867N"));

		assert (ProjectOperations.updateProjectEmployees(P2.getCode(), employees) == 1):
			"Error in updateEmployeeFirstName, output is "+ProjectOperations.updateProjectEmployees(P2.getCode(), employees);

		assert (ProjectOperations.getProject(P2.getCode()).getName().equals("MB42")):
			"Error: the Project name were not update";

		assert (ProjectOperations.getProject(P2.getCode()).getLocation().equals("Zurich")):
			"Error: the Project LOCATION were not update";

		assert (ProjectOperations.getProject(P2.getCode()).getControllingDepartment().getCode().equals("ER")):
			"Error: the Project Controlling Department were not update";

		for(int i = 0; i < ProjectOperations.getProject(P2.getCode()).getEmployees().size(); i++)
			assert (ProjectOperations.getProject(P2.getCode()).getEmployees().get(i).getSSN().equals( employees.get(i).getSSN())):
				"Error: the Project employees were not update";

		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testUpdateProject() PASSED");	
	}
	
	@org.junit.jupiter.api.Test
	public void testRemoveProject() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[0] == 8):
			"There was a error during createEmployee operation";
		Project P3 =  ProjectOperations.getProject("MB03");
		Project P8 =  ProjectOperations.getProject("RT02");
		
		assert (P8 != null): "A Project that should be in the db isn't inside!";
		
		assert (ProjectOperations.removeProject(P8.getCode()) == 1):
			"An error occured during remove project operation";
		
		assert (ProjectOperations.removeProject(P8.getCode()) == -1):
			"This project has already been removed";
		
		assert (ProjectOperations.removeProject(P3.getCode()) == 1):
			"An error occured during remove project operation"; 
		
		assert (ProjectOperations.getProject(P8.getCode()) == null):
			P8.getCode() + " " + P8.getName() + ": this Project should not be in the db";
		
		assert (ProjectOperations.getProject(P3.getCode()) == null):
			"Project MB03 should not be in the db";
		
		System.out.println("testemoveProject() PASSED");
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
	}

}
