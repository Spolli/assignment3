package Program;


import java.util.GregorianCalendar;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import Classes.Dependent;
import crudOperations.DbOperations;
import crudOperations.DependentOperations;
import crudOperations.EmployeeOperations;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TestDependentOperations {

	@org.junit.jupiter.api.Test
	public void testInsertDependent() {
		int [] data = DbOperations.insertAllData();

		assert(data[3] == 7):
			"There was a error during createEmployee operation";
		Dependent W1 = DependentOperations.getDependent("GLLMRC07E65F456V");
		Dependent W2 = DependentOperations.getDependent("DRGFRC12E85A456V");
		Dependent W3 = DependentOperations.getDependent("PNGMRO45E54B264A");
		Dependent W4 = DependentOperations.getDependent("DNZMRL42T45T243B");
		Dependent W5 = DependentOperations.getDependent("VLLFB012R43B556J");
		Dependent W6 = DependentOperations.getDependent("SCCSTF23G44N423H");
		Dependent W7 = DependentOperations.getDependent("RILMNO34R54N242K");
		
		assert(DependentOperations.getDependent(W1.getSSN()) != null):
			W1.getSSN() + " " +W1.getFirstName() + " " +W1.getLastName() + " dependent should be in the database!";
		
		assert(DependentOperations.getDependent(W2.getSSN()) != null):
			W2.getSSN() + " " +W2.getFirstName() + " " +W2.getLastName() + " dependent should be in the database!";
		
		assert(DependentOperations.getDependent(W3.getSSN()) != null):
			W3.getSSN() + " " +W3.getFirstName() + " " +W3.getLastName() + " dependent should be in the database!";
		
		assert(DependentOperations.getDependent(W4.getSSN()) != null):
			W4.getSSN() + " " +W4.getFirstName() + " " +W4.getLastName() + " dependent should be in the database!";
		
		assert(DependentOperations.getDependent(W5.getSSN()) != null):
			W5.getSSN() + " " +W5.getFirstName() + " " +W5.getLastName() + " dependent should be in the database!";
			
		assert(DependentOperations.getDependent(W6.getSSN()) != null):
			W6.getSSN() + " " +W6.getFirstName() + " " +W6.getLastName() + " dependent should be in the database!";
		
		assert(DependentOperations.getDependent(W7.getSSN()) != null):
			W7.getSSN() + " " +W7.getFirstName() + " " +W7.getLastName() + " dependent should be in the database!";
		
		assert(DependentOperations.getDependent("PLZBRN86A23D564G") == null):
			"This dependent shouldn't be in the database!";
		
		assert(DependentOperations.getDependentsByEmployee(EmployeeOperations.getEmployee("DNGMRC05F77E796P")).size() == 2);
		
		assert(DependentOperations.getDependentsByFirstName("Mario").size() == 2);
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testInsertDependent() PASSED");
	}
	
	@org.junit.jupiter.api.Test
	public void testUpdateDependent() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[3] == 7):
			"There was a error during createEmployee operation";
		Dependent W1 = DependentOperations.getDependent("GLLMRC07E65F456V");
		
		assert (W1 != null):
			"A dependent that should be in the db isn't inside!";
		
		assert(DependentOperations.updateDependentFirstName(W1.getSSN(), "Mario") == 1):
			"Error in updateDependentFirstName, output is "+DependentOperations.updateDependentFirstName(W1.getSSN(), "Mario");
		
		assert(DependentOperations.updateDependentFirstName(W1.getSSN(), "PierMarioGiancarloLuigiFrancesco") == -1):
			"Error in updateDependentFirstName, newFirstName too long";
		
		assert(DependentOperations.updateDependentLastName(W1.getSSN(), "Palazzi") == 1):
			"Error in updateDependentLastName, output is "+DependentOperations.updateDependentLastName(W1.getSSN(), "Palazzi");

		assert(DependentOperations.updateDependentLastName(W1.getSSN(), "Parascandolondandolazzi") == -1):
			"Error in updateDependentLastName,newLastName too long";

		assert (DependentOperations.updateDependentDOB(W1.getSSN(), new GregorianCalendar(1942, 05, 01)) == 1):
			"Error in updateDependentDOB, output is "+DependentOperations.updateDependentDOB(W1.getSSN(), new GregorianCalendar(1977, 04, 01));
		
		assert (DependentOperations.updateDependentDOB(W1.getSSN(), null) == -1):
			"Error in updateDependentDOB, invalid date";
		
		assert (DependentOperations.getDependent(W1.getSSN()).getFirstName().equals("Mario")):
			"Error: the dependent name were not update";
		
		assert (DependentOperations.getDependent(W1.getSSN()).getLastName().equals("Palazzi")):
			"Error: the dependent name were not update";
		
		assert (DependentOperations.getDependent(W1.getSSN()).getDateOfBirth().equals(new GregorianCalendar(1942, 05, 01))):
			"Error: the dependent DOB were not update";
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testUpdateDependent() PASSED");	
	}
	
	@org.junit.jupiter.api.Test
	public void testRemoveDependent() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[3] == 7):
			"There was a error during createEmployee operation";		
		Dependent W4 = DependentOperations.getDependent("DNZMRL42T45T243B");
		Dependent W5 = DependentOperations.getDependent("VLLFB012R43B556J");
		
		assert (W4 != null):
			"A Dependent that should be in the db isn't inside!";
		
		assert (DependentOperations.removeDependent(W4.getSSN()) == 1):
			"An error occured during remove Dependent operation";
		
		assert (DependentOperations.removeDependent(W4.getSSN()) == -1):
			"This Dependent has been already removed";
		
		assert (DependentOperations.removeDependent(W5.getSSN()) == 1):
			"An error occured during remove Dependent operation";
		
		assert (DependentOperations.removeDependent("PLZBRN28A96A794N") == -1):
			"This Dependent isn't in the database";
		
		assert (DependentOperations.getDependent(W4.getSSN()) == null):
			W4.getSSN() + " " + W4.getFirstName() + ": this Dependent should not be in the db";
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		
		System.out.println("testRemoveDependent() PASSED");	
		
		
			
	}
	
}
