package Program;

import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import Classes.Employee;
import Classes.Project;
import crudOperations.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TestEmployeeOperations {

	@org.junit.jupiter.api.Test
	public void testInsertEmployee() {
		int [] data = DbOperations.insertAllData();

		assert(data[1] == 7):
			"There was a error during createEmployee operation";
		Employee E1 = EmployeeOperations.getEmployee("DNGMRC05F77E796P");
		Employee E2 = EmployeeOperations.getEmployee("CRSSMN02E87R294O");
		Employee E3 = EmployeeOperations.getEmployee("SNLSRN05B66E856P");
		Employee E4 = EmployeeOperations.getEmployee("DRGFRC12I88E365L");
		Employee E5 = EmployeeOperations.getEmployee("MSTSLV27I59E745T");
		Employee E6 = EmployeeOperations.getEmployee("GNDLCU14M65E867N");
		Employee E7 = EmployeeOperations.getEmployee("GLLETR13C94W453G");
		
		assert(EmployeeOperations.getEmployee(E1.getSSN()) != null):
			E1.getSSN() + " " +E1.getFirstName() + " " +E1.getLastName() + " employee should be in the database!";
		
		assert(EmployeeOperations.getEmployee(E2.getSSN()) != null):
			E2.getSSN() + " " +E2.getFirstName() + " " +E2.getLastName() + " employee should be in the database!";
		
		assert(EmployeeOperations.getEmployee(E3.getSSN()) != null):
			E3.getSSN() + " " +E3.getFirstName() + " " +E3.getLastName() + " employee should be in the database!";
		
		assert(EmployeeOperations.getEmployee(E4.getSSN()) != null):
			E4.getSSN() + " " +E4.getFirstName() + " " +E4.getLastName() + " employee should be in the database!";
		
		assert(EmployeeOperations.getEmployee(E5.getSSN()) != null):
			E5.getSSN() + " " +E5.getFirstName() + " " +E5.getLastName() + " employee should be in the database!";
			
		assert(EmployeeOperations.getEmployee(E6.getSSN()) != null):
			E6.getSSN() + " " +E6.getFirstName() + " " +E6.getLastName() + " employee should be in the database!";
		
		assert(EmployeeOperations.getEmployee(E7.getSSN()) != null):
			E7.getSSN() + " " +E7.getFirstName() + " " +E7.getLastName() + " employee should be in the database!";
		
		assert(EmployeeOperations.getEmployee("PLZBRN56E47W345E") == null):
			E6.getSSN() + " " +E6.getFirstName() + " " +E6.getLastName() + " employee should be in the database!";
		
		assert(EmployeeOperations.getEmployeesByGreaterSalary(50000).size() == 5);
		
		assert(EmployeeOperations.getEmployeesByDepartment(DepartmentOperations.getDepartment("MB")).size() == 3);
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testInsertEmploy() PASSED");
	}
	
	@org.junit.jupiter.api.Test
	public void testUpdateEmployee() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[1] == 7):
			"There was a error during createEmployee operation";
		Employee E1 = EmployeeOperations.getEmployee("DNGMRC05F77E796P");
				
		assert (E1 != null): "An employee that should be in the db isn't inside!";
		
		assert(EmployeeOperations.updateEmployeeFirstName(E1.getSSN(), "Maria") == 1):
			"Error in updateEmployeeFirstName, output is "+EmployeeOperations.updateEmployeeFirstName(E1.getSSN(), "Maria");

		assert(EmployeeOperations.updateEmployeeFirstName(E1.getSSN(), "Mariannannannannannanna") == -1):
			"Error in updateEmployeeFirstName, newFirstName is too long";

		assert(EmployeeOperations.updateEmployeeLastName(E1.getSSN(), "Vitali") == 1):
			"Error in updateEmployeeLastName, output is "+EmployeeOperations.updateEmployeeLastName(E1.getSSN(), "Vitali");

		assert(EmployeeOperations.updateEmployeeLastName(E1.getSSN(), "Vitali-Baldini-Mazzuchetti") == -1):
			"Error in updateEmployeeLastName, newLastName is too long";

		assert(EmployeeOperations.updateEmployeeDOB(E1.getSSN(), new GregorianCalendar(1942, 05, 01)) == 1):
			"Error in updateEmployeeDOB, output is "+EmployeeOperations.updateEmployeeDOB(E1.getSSN(), new GregorianCalendar(1942, 05, 01));
		
		assert(EmployeeOperations.updateEmployeeDOB(E1.getSSN(), null) == -1):
			"Error in updateEmployeeDOB, invalid date ";
		
		assert(EmployeeOperations.updateEmployeeSex(E1.getSSN(), "female") == 1):
			"Error in updateEmployeeSex, output is "+EmployeeOperations.updateEmployeeSex(E1.getSSN(), "female");
		
		assert(EmployeeOperations.updateEmployeeSex(E1.getSSN(), "fem") == -1):
			"Error in updateEmployeeSex, sex must be male or female";
		
		assert(EmployeeOperations.updateEmployeeAddress(E1.getSSN(), "123rd Fasula Street") == 1):
			"Error in updateEmployeeAddress, output is "+EmployeeOperations.updateEmployeeAddress(E1.getSSN(), "123rd Fasula Street");
		
		assert(EmployeeOperations.updateEmployeeAddress(E1.getSSN(), "123rd Roberspierre and Jaques Rosseau Street") == -1):
			"Error in updateEmployeeAddress, address is too long";
		
		assert(EmployeeOperations.updateEmployeeSalary(E1.getSSN(), 34434) == 1):
			"Error in updateEmployeeSalary, output is "+EmployeeOperations.updateEmployeeSalary(E1.getSSN(), 34434);
		
		assert(EmployeeOperations.updateEmployeeSalary(E1.getSSN(), -34434) == -1):
			"Error in updateEmployeeSalary, this value must be greather than 0";
		
		assert(EmployeeOperations.updateEmployeeSupervisor(E1.getSSN(), EmployeeOperations.getEmployee("DNGMRC05F77E796P")) == 1):
			"Error in updateEmployeeSupervisor, output is "+EmployeeOperations.updateEmployeeSupervisor(E1.getSSN(), EmployeeOperations.getEmployee("GLLETR13C94W453G"));
		
		assert(EmployeeOperations.updateEmployeeSupervisor(E1.getSSN(), EmployeeOperations.getEmployee("PLZBRN55F77E796P")) == -1):
			"Error in updateEmployeeSupervisor, the Supervisor doesn't exist";
		
		List <Project> L1 = new LinkedList();
		L1.add(ProjectOperations.getProject("MB01"));
		L1.add(ProjectOperations.getProject("MB02"));
		L1.add(ProjectOperations.getProject("MB03"));
		
		assert(EmployeeOperations.updateEmployeeProjects(E1.getSSN(), L1) == 1):
			"Error in updateEmployeeProjects, output is "+EmployeeOperations.updateEmployeeProjects(E1.getSSN(), L1);
		
		assert(EmployeeOperations.updateEmployeeDepartment(E1.getSSN(), DepartmentOperations.getDepartment("SR")) == 1):
			"Error in updateEmployeeDepartment, output is "+EmployeeOperations.updateEmployeeDepartment(E1.getSSN(), DepartmentOperations.getDepartment("SR"));
		
		assert(EmployeeOperations.updateEmployeeDepartment(E1.getSSN(), DepartmentOperations.getDepartment("QW")) == -1):
			"Error in updateEmployeeDepartment, output is "+EmployeeOperations.updateEmployeeDepartment(E1.getSSN(), DepartmentOperations.getDepartment("SR"));
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getFirstName().equals("Maria")):
			"Error: the employee Firstname were not update";
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getLastName().equals("Vitali")):
			"Error: the employee Lastname were not update";
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getDateOfBirth().equals( new GregorianCalendar(1942, 05, 01))):
			"Error: the employee DOB were not update";
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getSex().equals("female")):
			"Error: the employee sex were not update";
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getAddress().equals("123rd Fasula Street")):
			"Error: the employee address were not update";
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getSalary() == 34434):
			"Error: the employee salary were not update";
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getSupervisor().getSSN().equals("DNGMRC05F77E796P")):
			"Error: the employee supervisor were not update";
		
		assert (EmployeeOperations.getEmployee(E1.getSSN()).getDepartment().getCode().equals("SR")):
			"Error: the employee Department were not update";
		
		for(int i = 0; i < EmployeeOperations.getEmployee(E1.getSSN()).getProjects().size(); i++)
			assert (EmployeeOperations.getEmployee(E1.getSSN()).getProjects().get(i).getCode() == L1.get(i).getCode()):
				"Error: the employee projects were not update";
		
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
		System.out.println("testUpdateEmployee() PASSED");	
	}
	
	@org.junit.jupiter.api.Test
	public void testRemoveEmployee() {
		
		int [] data = DbOperations.insertAllData();

		assert(data[1] == 7):
			"There was a error during createEmployee operation";
		Employee E4 = EmployeeOperations.getEmployee("DRGFRC12I88E365L");
		Employee E7 = EmployeeOperations.getEmployee("GLLETR13C94W453G");
		
		assert (E7 != null): "An Employee that should be in the db isn't inside!";
		
		assert (EmployeeOperations.removeEmployee(E7.getSSN()) == 1): "An error occured during remove employee operation";
		
		assert (EmployeeOperations.removeEmployee(E7.getSSN()) == -1): "This employee has already been removed";
		
		assert (EmployeeOperations.removeEmployee(E4.getSSN()) == -1): "Can't remove this employee because it's related to another entity";
		
		assert (EmployeeOperations.removeEmployee("PLZBRN28A96A794P") == -1):"This employee isn't in the database";
		
		assert (EmployeeOperations.getEmployee(E7.getSSN()) == null):
			E7.getSSN() + " " + E7.getFirstName() + ": this Dependent should not be in the db";
		
		
		System.out.println("testRemoveEmployee() PASSED");		
		
		try {
			DbOperations.deleteAllData();
		}
		catch(Exception e){
			System.out.println("Error while dropping all data");
		}
	}
}
