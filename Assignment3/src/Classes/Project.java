package Classes;
import java.util.List;
import javax.persistence.*;
@Entity
@Table(name="Project")

public class Project implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		@Id 
		@Column(name="Id",length = 10)
		private String code;
		@Column(name="Name", length = 20)
		private String name;
		@Column(name="Location", length = 20)
		private String location;
		@ManyToOne(cascade = CascadeType.ALL) @JoinColumn(name="controllingDepartment")
		private Department controllingDepartment; 
		@ManyToMany(cascade = CascadeType.ALL)
		private List<Employee> employees;
		
		public Project() {
			 
		}
				 
		public Project(String code, String name, String location, Department controllingDepartment, List<Employee> employees) {
			 this.code = code;
			 this.name = name;
			 this.location = location;
			 this.controllingDepartment = controllingDepartment;
			 this.employees = employees;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public List<Employee> getEmployees() {
			return employees;
		}

		public void setEmployees(List<Employee> employees) {
			this.employees = employees;
		}

		public Department getControllingDepartment() {
			return controllingDepartment;
		}

		public void setControllingDepartment(Department controllingDepartment) {
			this.controllingDepartment = controllingDepartment;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}
		 
}
