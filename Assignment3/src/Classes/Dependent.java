package Classes;
import javax.persistence.*;
import java.util.GregorianCalendar;
@Entity
@Table(name="Dependent")

public class Dependent implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		@Id
		@Column(name="Id" , length = 18)
		private String SSN;;
		@Column(name="FirstName", length = 20)
		private String firstName;
		@Column(name="LastName", length = 20)
		private String lastName;
		@Column(name="DateOfBirth")
		@Temporal(TemporalType.DATE)
		private GregorianCalendar dateOfBirth;
		@ManyToOne (cascade = CascadeType.ALL) @JoinColumn(name="employee")
		private Employee employee;
		public Dependent() {
			 
		}
				 
		public Dependent(String SSN, String firstName, String lastName, GregorianCalendar dateOfBirth, Employee employee) {
			 this.SSN = SSN;
			 this.firstName = firstName;
			 this.lastName = lastName;
			 this.dateOfBirth = dateOfBirth;
			 this.employee = employee;
		}
		 
		public String getSSN() {
			return SSN;
		}

		public void setSSN(String SSN) {
			this.SSN = SSN;
		}

		public Employee getEmployee() {
			return employee;
		}

		public void setEmployee(Employee employee) {
			this.employee = employee;
		}

		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public GregorianCalendar getDateOfBirth() {
				return dateOfBirth;
		}
		public void setDateOfBirth(GregorianCalendar dateOfBirth) {
				this.dateOfBirth = dateOfBirth;
		}
}
