package Classes;
import javax.persistence.*;
@Entity
@Table(name="Department")

public class Department implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		@Id
		@Column(name="Id", length = 10)
		private String code;
		@Column(name="Name", length = 20)
		private String name;
		@Column(name="Location", length = 20)
		private String location;
		@Column(name="EmployeeNumber")
		private long employeeNumber;
		@OneToOne(cascade = CascadeType.ALL) @JoinColumn(name="Manager")
		private Employee manager;
		public Department() {
			 
		}
				 
		public Department(String code, String name, String location, long employeeNumber, Employee manager) {
			 this.code = code;
			 this.name = name;
			 this.location = location;
			 this.employeeNumber = employeeNumber;
			 this.manager = manager;
		}
		
		public String getName() {
			return name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public Employee getManager() {
			return manager;
		}

		public void setManager(Employee manager) {
			this.manager = manager;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public long getEmployeeNumber() {
			return employeeNumber;
		}

		public void setEmployeeNumber(long employeeNumber) {
			this.employeeNumber = employeeNumber;
		}
		 
}
