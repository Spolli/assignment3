package Classes;
import java.util.List;
import javax.persistence.*;
import java.util.GregorianCalendar;
@Entity
@Table(name="Employee")
public class Employee implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		@Id
		@Column(name="Id", length = 18)
		private String SSN;
		@Column(name="FirstName", length = 20)
		private String firstName;
		@Column(name="LastName", length = 20)
		private String lastName;
		@Column(name="DateOfBirth")
		@Temporal(TemporalType.DATE)
		private GregorianCalendar dateOfBirth;
		@Column(name="Sex" , length = 7)
		private String sex;
		@Column(name="Address", length = 50)
		private String address;
		@Column(name="Salary")
		private double salary;
		@ManyToOne(cascade = CascadeType.ALL) @JoinColumn(name="Department")
		private Department department;
		@ManyToOne(cascade = CascadeType.ALL) @JoinColumn(name="Supervisor")
		private Employee supervisor;
		@ManyToMany (mappedBy="employees", cascade = CascadeType.ALL)
		private List<Project> projects;
		
		public Employee() {
			 
		}
				 
		public Employee(String SSN, String firstName, String lastName, GregorianCalendar dateOfBirth, String sex, String address, double salary, Department department, Employee supervisor, List<Project> projects) {
			 this.SSN = SSN;
			 this.firstName = firstName;
			 this.lastName = lastName;
			 this.dateOfBirth = dateOfBirth;
			 this.sex = sex;
			 this.address = address;
			 this.salary = salary;
			 this.department = department;
			 this.supervisor = supervisor;
			 this.projects = projects;
		}
		
		public GregorianCalendar getDateOfBirth() {
			return dateOfBirth;
		}

		public void setDateOfBirth(GregorianCalendar dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}

		public Department getDepartment() {
			return department;
		}

		public void setDepartment(Department department) {
			this.department = department;
		}

		public Employee getSupervisor() {
			return supervisor;
		}

		public void setSupervisor(Employee supervisor) {
			this.supervisor = supervisor;
		}

		public List<Project> getProjects() {
			return projects;
		}

		public void setProjects(List<Project> projects) {
			this.projects = projects;
		}

		public String getSSN() {
			return SSN;
		}

		public void setSSN(String SSN) {
			this.SSN = SSN;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getSex() {
			return sex;
		}
		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public double getSalary() {
			return salary;
		}

		public void setSalary(double salary) {
			this.salary = salary;
		}
}
